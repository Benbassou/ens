// attendre que l'output buffer reçoive une donnée du clavier

attente:
    inb $0x64, %al   // AL = status register
    testb $0x01, %al // teste le bit$_0$ (OUTB) du registre AL
    jz attente 	     // bit$_0 = 0 \Rightarrow$ rien dans l'output buffer

// l'output buffer contient quelque chose : le lire

    inb $0x60, %al   // AL = code de la touche
